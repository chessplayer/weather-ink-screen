## 请勿用于大规模商业用途，禁止售卖资料！
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V12-3.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V12-4.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V12-0.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V12-1.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V12-2.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/qzg1.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/qzg3.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/qzg4.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/fhqzg.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/fh.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/013-09.jpg) 
## 新增天数倒计时等多项功能 [点击下载](https://gitee.com/Lichengjiez/weather-ink-screen/releases)
## 前置光版本完成！开源PCB项目链接[https://oshwhub.com/jie326513988/tian-qi-mo-shui-ping](https://oshwhub.com/jie326513988/tian-qi-mo-shui-ping)
## 下一硬件版本加入BL8025T时钟芯片（已打板测试正常，固件也写好了）
## 适配16MB FLASH的固件已写好，建议替换型号为25Q128JVSQ(16M)

### 串口不正常解决方案
* 磁珠改为0R电阻或直连
* ESD需换成SMS05C
* 确保PCB要求的100uf钽电容有焊接上

### 前置光PCB
* 兼容前置光墨水屏-大连佳显T5D，使用普通墨水屏不焊前置光驱动电路即可（前置光外壳待上传）
* 添加透明亚克力后盖（已上传，含亚克力尺寸）
* 整机休眠低至0.02ma
* 温湿度芯片-SHT30
* ESD芯片-SMS05C
    * SRV05-4为双向不适合此电路，其他双向ESD芯片也不行
    * 因为双向ESD芯片在不插USB时会有电漏到ch340的开关MOS，导致ch340工作浪费电
* 性能更佳封装更小的LDO-ME6210A33M3G
* MLCC电容阵列
* 加长焊盘，物件间距合理，不再手残
* 必须插电池并打开拨动开关进行烧录程序，不支持USB&锂电池切换电源
* 前置光亮度调到最亮时对屏幕刷新有少许干扰，其他时候正常，不需要前置光不焊不影响
* 部分硬件有参考[HalfSweet](https://oshwhub.com/HalfSweet/29-EPaper-Thermo-hygrometer)和[DUCK](https://oshwhub.com/duck/esp8266-weather-station-epaper)，感谢！

### 介绍
* 默认支持2.9寸墨水屏，代号029A01<br>
* 如需其他同尺寸的屏幕，请留言<br>
* 观看视频：[https://www.bilibili.com/video/BV1e64y1Q7mA](https://www.bilibili.com/video/BV1e64y1Q7mA)
* 使用Arduino开发，使用到的库GxEPD2、U8g2_for_Adafruit_GFX、NTPClient、ArduinoJson、ESP_EEPROM。<br>
* 使用心知天气个人免费版KEY（20次/分钟），需要自己去申请，然后在配网界面输入即可。<br>
* 提供适合3D打印的外壳文件<br>
* 休眠电流0.026-0.04ma，工作电流120-70ma，瞬态电流500ma
* 硬件使用[DUCK的天气墨水屏项目](http://oshwhub.com/duck/esp8266-weather-station-epaper)。<br>
* 改良版硬件，地址 [https://oshwhub.com/HalfSweet/29-EPaper-Thermo-hygrometer](https://oshwhub.com/HalfSweet/29-EPaper-Thermo-hygrometer) （有BUG待修复）

### 功能简介
* 天气模式
    * 天气实况、未来2天天气
    * 紫外线强度、室外环境湿度、风力等级
    * 中间显示一句话，网络获取或自定义
    * 电量显示，电压或百分比
    * 室内温湿度显示（需硬件支持sht30芯片）
    * 自定义夜间不更新/更新
* 阅读模式
    * 多达2.8mb的使用空间，最多可存3本书
    * 使用索引方式，准确计算页数，可任意跳转，无限上一页
    * 记忆功能，自动恢复上一次看的书籍和页数
    * 电量显示
    * 显示时间（构建中）
* 时钟模式
    * 超大数字显示，不再眼瞎
    * 自定义显示模式，日期/简洁
    * 自定义校准间隔（已完成，已发布）
    * 自定义是否开启强制校准（已完成，已发布）
    * 自定义离线校准补偿
* 配网模式
    * 自动、手动选择配置网络
    * 配置天气KEY和城市
    * 上传文件
    * 文件管理器
    * 设置自定义图片
    * 丰富的设置项
    * 屏幕实时消息回传提示
    * 预留OTA接口
* 自定义图片模式
    * 显示自定义的bmp图片，需到配网-文件管理器启用
* 天数倒计时（已完成，已发布）
* B站粉丝显示（已完成，已发布）
* 所有模式下低电量会提示并永久休眠，小于等于3.3V

### 按键操作逻辑
* 正视图从左到右依次为 按键1 按键2 按键3 (按键3-GPIO5 按键2-GPIO0)
* 按住按键3不放，再按复位按键（按键1），即可进入模式选择界面
* 所有界面的按键操作逻辑为：
    * a.单独短按为切换选项
    * b.长按按键3为确认操作或调出菜单（原组合按键取消）
* 按键2不可按得太频繁，不能在屏幕刷新的时候按，会导致屏幕死机，原因是按键2与屏幕刷新共用一个io口

### 注意事项，请耐心看完
* 观看视频 [https://www.bilibili.com/video/BV1kQ4y1d7Jj](https://www.bilibili.com/video/BV1kQ4y1d7Jj)
* 原版U8g2_for_Adafruit_GFX库无法使用大字库，故更改了库，自行到码云或群里下载
* 其他库均可在库管理器下载的到
* 无法连接wifi可能是被路由器拉黑或网络差，天线附近需要净空不能有飞线，电池挡住天线也可能会有影响
* 无法获取天气信息请检查城市名是否填对，免费用户只能查看到地级市
* 误低压休眠的请检查电池测量电路是否正常，电池电压是否大于3.25V（搭板的玩家自己给A0加上分压电路接上5V，分压后不能超过1V，否则烧ADC）
* 如原版的MOS管（排线附近）使用起来有问题可用cj3400代替
* 如无法连接8266的热点或无法打开配网页，请检查手机是否开启了智能选网模式
* 电压低进入配网重启的话就在LDO输出3.3V处并多点电容，电解电容也行
* 电池可以用：303450-500mah（适配本项目隐藏电池的背透外壳），602650-650mah（适配本项目外壳），601550-450mah（适配本项目的背透外壳），902030-500mah（DUCK&半糖的外壳）
* [GxEPD2](https://github.com/ZinggJM/GxEPD2)，[该库适配大多数大连佳显屏幕](https://goodlcd.taobao.com/shop/view_shop.htm?spm=a230r.1.14.39.7a293567D3cz3Y&user_number_id=151859855)
* [U8g2_for_Adafruit_GFX](https://github.com/olikraus/U8g2_for_Adafruit_GFX)
* [ArduinoJson](https://github.com/bblanchon/ArduinoJson)
* [NTPClient](https://github.com/arduino-libraries/NTPClient)
* [ESP_EEPROM](https://github.com/jwrw/ESP_EEPROM)
* [ClosedCube_SHT31D](https://github.com/closedcube/ClosedCube_SHT31D_Arduino)

### 已知BUG
* ~开机载入数据有小几率会重启系统，EXCCAUSE Code(3),加载或存储期间的处理器内部物理地址或数据错误？~
* 在配网页面连接无效的的WIFI会卡一段时间，有相应提示。可能是硬件问题，无法同时进行STA和AP的收发？等待提示连接失败即可操作其他。
* 进入配网模式有几率重启，多试几次就好。
* ~文件管理器有几率抽风，重启系统即可~ （已修复）
* 无法建立索引检查TXT文本结尾不能有换行
* 16MB的固件最大只支持9.9MB的TXT文本
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/%E8%B6%85%E8%AF%A6%E7%BB%868266%E7%83%A7%E5%BD%95%E6%95%99%E7%A8%8B.jpg)
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/peizhi.png)
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/16mbpeizhi.png)
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/wenjianguanliqi.jpeg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/pw%20(1).jpg)
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/pw%20(2).jpg)
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/dianliu.jpg) 

